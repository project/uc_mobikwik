Installation procedure:
Go to 'Modules' and install the Mobikwik wallet module.
Go to to 'Store => Configuration => Payment methods" 
( URL: admin/store/settings/payment ).
Enable 'Mobikwik Wallet Payment Gateway' method.
Go to the settings of the wallet 
( URL: admin/store/settings/payment/method/mobikwik ).
Enter your Mobikwik Wallet Merchant id and Secret key. 
( For Test Mode, 
enter Merchant id as 'MBK9002'
and Secret Key as 'ju6tygh7u7tdg554k098ujd5468o' )
- For test mode, 
please enter value as '0' for mode. 
For live mode, please enter value 1.
- Click on Save.

Go to "Store => Configuration => Countries and addresses" 
(admin/store/settings/countries/fields).
- Enable "Phone number" field
and make it as required one.

Note: This module just redirects the customers to the Mobikwik site 
where the user can make the payment and redirects to our site. 
The flow is similar Paypal WPS.
We can not add money to the mobikwik by using our module.  
