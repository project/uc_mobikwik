<?php

/**
 * @file
 * Integrates Mobikwik wallet with Ubercart.
 */

/**
 * Implements hook_menu().
 */
function uc_mobikwik_menu() {
  $items['uc_mobikwik/mobikwik_redirect/%'] = array(
    'title' => 'Mobikwik Redirect',
    'page callback' => 'uc_mobikwik_redirect',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['uc_mobikwik/complete/%'] = array(
    'title' => 'Mobikwik payment complete',
    'page callback' => 'uc_mobikwik_complete',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['uc_mobikwik/cancel'] = array(
    'title' => 'Mobikwik payment cancelled',
    'page callback' => 'uc_mobikwik_cancel',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_uc_payment_method().
 */
function uc_mobikwik_uc_payment_method() {

  $methods[] = array(
    'id' => 'mobikwik',
    'name' => t('Mobikwik Wallet Payment Gateway'),
    'title' => t('Mobikwik Wallet'),
    'review' => t('Mobikwik'),
    'desc' => t('Redirect users to submit payments through Mobikwik Wallet.'),
    'callback' => 'uc_mobikwik_payment_method',
    'redirect' => 'uc_mobikwik_form',
    'weight' => 1,
    'checkout' => FALSE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Handles the Mobikwik Payment.
 */
function uc_mobikwik_payment_method($op, &$arg1) {

  switch ($op) {
    case 'settings':
      $form['uc_mobikwik_merchant_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Merchant ID'),
        '#default_value' => variable_get('uc_mobikwik_merchant_id', ''),
      );

      $form['uc_mobikwik_secret_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Secret Key'),
        '#default_value' => variable_get('uc_mobikwik_secret_key', ''),
      );

      $form['uc_mobikwik_mode'] = array(
        '#type' => 'select',
        '#title' => t('Mobikwik Mode'),
        '#options' => array(
          '0' => ('Test'),
          '1' => ('Live'),
        ),
        '#default_value' => variable_get('uc_mobikwik_mode', 0),
      );
      return $form;
  }
}

/**
 * Returns the form elements for the form.
 */
function uc_mobikwik_form($form, &$form_state, $order) {

  $amount = $order->order_total;
  $returnUrl = url('uc_mobikwik/mobikwik_redirect/' . $order->order_id, array('absolute' => TRUE));
  $merchant_id = variable_get('uc_mobikwik_merchant_id', '');
  $secret_key = variable_get('uc_mobikwik_secret_key', '');
  $mode = variable_get('uc_mobikwik_mode', 0);
  $orderid = $order->order_id;
  $email = $order->primary_email;
  $cell = $order->{'billing_phone'};
  $all = "'" . $cell . "''" . $email . "''" . $amount . "''" . $orderid . "''" . $returnUrl . "''" . $merchant_id . "'";
  $checksum = uc_mobikwik_calculate_checksum($secret_key, $all);

  $form['mid'] = array(
    '#type' => 'hidden',
    '#value' => uc_mobikwik_sanitized_param($merchant_id),
  );
  $form['orderid'] = array(
    '#type' => 'hidden',
    '#value' => uc_mobikwik_sanitized_param($orderid),
  );
  $form['redirecturl'] = array(
    '#type' => 'hidden',
    '#value' => $returnUrl,
  );
  $form['email'] = array(
    '#type' => 'hidden',
    '#value' => uc_mobikwik_sanitized_param($email),
  );
  $form['cell'] = array(
    '#type' => 'hidden',
    '#value' => uc_mobikwik_sanitized_param($cell),
  );
  $form['amount'] = array(
    '#type' => 'hidden',
    '#value' => uc_mobikwik_sanitized_param($amount),
  );
  $form['merchantname'] = array(
    '#type' => 'hidden',
    '#value' => 'mobikwik',
  );
  $form['checksum'] = array(
    '#type' => 'hidden',
    '#value' => $checksum,
  );

  if ($mode == 0) {
    $form['#action'] = 'https://test.mobikwik.com/wallet';
  }
  elseif ($mode == 1) {
    $form['#action'] = 'https://www.mobikwik.com/wallet';
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit order'),
  );

  return $form;
}

/**
 * Page callback for Mobikwik redirect.
 */
function uc_mobikwik_redirect($order_id = 0) {

  $orderid = $_REQUEST['orderid'];
  $statuscode = $_REQUEST['statuscode'];
  $statusmessage = $_REQUEST['statusmessage'];
  $merchant_id = $_REQUEST['mid'];
  $amount = $_REQUEST['amount'];
  $checksum = $_REQUEST['checksum'];

  $mid = variable_get('uc_mobikwik_merchant_id', '');
  $secret_key = variable_get('uc_mobikwik_secret_key', '');
  $mode = variable_get('uc_mobikwik_mode', 0);

  $order = uc_order_load($order_id, FALSE);
  $order_total = $order->order_total;

  $allParamValue = "'" . $statuscode . "''" . $orderid . "''" . $amount . "''" . $statusmessage . "''" . $merchant_id . "'";

  if ($checksum != NULL) {
    $isChecksumValid = uc_mobikwik_verify_checksum($checksum, $allParamValue, $secret_key);
  }

  // Validate if the order was correctly done.
  if ($order == FALSE) {
    watchdog('uc_mobikwik', 'Return attempted for non-existent order.', array(), WATCHDOG_ERROR);
    return;
  }

  if ($isChecksumValid) {
    if ($statuscode == "0") {
      $checkstatus = uc_mobikwik_verify_transaction($mid, $order_id, $order_total, $secret_key, $mode);
      $received_amount = $checkstatus['amount'];
      $received_orderid = $checkstatus['orderid'];
      $flag = $checkstatus['flag'];
      if (($flag == TRUE) && ($received_orderid == $orderid) && ($received_amount == $amount)) {
        $comment = t('Mobikwik Wallet transaction checksum: @checksum', array('@checksum' => $checksum));
        uc_payment_enter($order_id, 'mobikwik', $amount, $order->uid, NULL, $comment);
        uc_cart_complete_sale($order);
        uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through Mobikwik Wallet.', array('@amount' => $amount, '@currency' => 'INR')), 'order', 'payment_received');
        uc_mobikwik_complete($order_id);
        return drupal_set_message(t('Thank you for shopping with us. Your account has been charged and your transaction is successful.'));
      }
      else {
        drupal_set_message(t('The transaction has been declined.'));
        drupal_goto('cart');
      }
    }
    else {
      drupal_set_message(t('The transaction has been declined.'));
      drupal_goto('cart');
    }
  }
  else {
    drupal_set_message(t('The transaction has been declined.'));
    drupal_goto('cart');
  }
}

/**
 * Page callback to handle a complete Mobikwik Payment sale.
 *
 * @param int $order_id
 *   The ID of the current order.
 */
function uc_mobikwik_complete($order_id = 0) {
  watchdog('Order Complete', print_r($order_id, TRUE));

  if (intval($_SESSION['cart_order']) != $order_id) {
    $_SESSION['cart_order'] = $order_id;
  }

  if (FALSE === uc_order_load($order_id, FALSE)) {
    drupal_goto('cart');
  }

  $_SESSION['do_complete'] = TRUE;
  drupal_goto('cart/checkout/complete');
}

/**
 * Page callback to handle a canceled Mobikwik Payment sale.
 */
function uc_mobikwik_cancel() {
  unset($_SESSION['cart_order']);
  drupal_set_message(t('Your Mobikwik payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));
  drupal_goto('cart');
}

/**
 * Calculate the checksum.
 */
function uc_mobikwik_calculate_checksum($secret_key, $all) {
  $hash = hash_hmac('sha256', $all, $secret_key);
  $checksum = $hash;
  return $checksum;
}

/**
 * Verify the checksum.
 */
function uc_mobikwik_verify_checksum($checksum, $all, $secret) {
  $cal_checksum = uc_mobikwik_calculate_checksum($secret, $all);
  $bool = 0;
  if ($checksum == $cal_checksum) {
    $bool = 1;
  }

  return $bool;
}

/**
 * Calculate the wallet checksum.
 */
function uc_mobikwik_calculate_wallet_checksum($merchantId, $secretKey, $orderId) {
  $algo = 'sha256';
  $checksum_string = "'{$merchantId}''{$orderId}'";
  $checksum = hash_hmac($algo, $checksum_string, $secretKey);
  return $checksum;
}

/**
 * Validate the wallet checksum.
 */
function uc_mobikwik_validate_checksum($statuscode, $orderid, $refid, $amount, $statusmessage, $ordertype, $working_key) {
  $algo = 'sha256';

  $checksum_string = "'{$statuscode}''{$orderid}''{$refid}''{$amount}''{$statusmessage}''{$ordertype}'";
  $checksum = hash_hmac($algo, $checksum_string, $working_key);
  return $checksum;
}

/**
 * Initiate the transaction.
 */
function uc_mobikwik_verify_transaction($merchant_id, $order_id, $amount, $working_key, $mode) {
  $return = array();
  $checksum = uc_mobikwik_calculate_wallet_checksum($merchant_id, $working_key, $order_id);

  if ($mode == 0) {
    $url = 'https://test.mobikwik.com/checkstatus';
  }
  elseif ($mode == 1) {
    $url = 'https://www.mobikwik.com/checkstatus';
  }

  $fields = "mid=$merchant_id&orderid=$order_id&checksum=$checksum&ver=2";

  if (!function_exists('curl_init')) {
    die('Sorry cURL is not installed!');
  }

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_TIMEOUT, 60);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
  $outputXml = curl_exec($ch);
  curl_close($ch);
  $outputXmlObject = simplexml_load_string($outputXml);
  $recievedChecksum = uc_mobikwik_validate_checksum(
    uc_mobikwik_sanitized_param($outputXmlObject->statuscode),
    uc_mobikwik_sanitized_param($outputXmlObject->orderid),
    uc_mobikwik_sanitized_param($outputXmlObject->refid),
    uc_mobikwik_sanitized_param($outputXmlObject->amount),
    uc_mobikwik_sanitized_param($outputXmlObject->statusmessage),
    uc_mobikwik_sanitized_param($outputXmlObject->ordertype),
    uc_mobikwik_sanitized_param($working_key));
  if (($order_id == $outputXmlObject->orderid) && $amount == $outputXmlObject->amount && ($outputXmlObject->checksum == $recievedChecksum)) {
    $return['statuscode'] = $outputXmlObject->statuscode;
    $return['orderid'] = $outputXmlObject->orderid;
    $return['refid'] = $outputXmlObject->refid;
    $return['amount'] = $outputXmlObject->amount;
    $return['statusmessage'] = $outputXmlObject->statusmessage;
    $return['ordertype'] = $outputXmlObject->ordertype;
    $return['checksum'] = $outputXmlObject->checksum;
    $return['flag'] = TRUE;
  }

  return $return;
}

/**
 * Sanitize parameters.
 */
function uc_mobikwik_sanitized_param($param) {
  $pattern[0] = "%,%";
  $pattern[1] = "%#%";
  $pattern[2] = "%\(%";
  $pattern[3] = "%\)%";
  $pattern[4] = "%\{%";
  $pattern[5] = "%\}%";
  $pattern[6] = "%<%";
  $pattern[7] = "%>%";
  $pattern[8] = "%`%";
  $pattern[9] = "%!%";
  $pattern[10] = "%\\$%";
  $pattern[11] = "%\%%";
  $pattern[12] = "%\^%";
  $pattern[13] = "%=%";
  $pattern[14] = "%\+%";
  $pattern[15] = "%\|%";
  $pattern[16] = "%\\\%";
  $pattern[17] = "%:%";
  $pattern[18] = "%'%";
  $pattern[19] = "%\"%";
  $pattern[20] = "%;%";
  $pattern[21] = "%~%";
  $pattern[22] = "%\[%";
  $pattern[23] = "%\]%";
  $pattern[24] = "%\*%";
  $pattern[25] = "%&%";
  $sanitizedParam = preg_replace($pattern, "", $param);
  return $sanitizedParam;
}
